package team.unstudio.udmc.core.item;

import net.minecraft.item.Item;

import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Created by winston_wang on 16/8/21.
 */
public class UDMCItem extends Item {
    private String regisrer_name;

    public UDMCItem(String name,String texture){
        setTextureName(texture);
        setUnlocalizedName(name);
    }
    public final void setRegisterName(String name){
        regisrer_name = name;
    }
    public final String getRegisterName(){
        return regisrer_name;
    }
    public final void register(){
        GameRegistry.registerItem(this,regisrer_name);
    }
}
