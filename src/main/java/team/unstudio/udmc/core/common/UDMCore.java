package team.unstudio.udmc.core.common;

import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import team.unstudio.udmc.core.block.UDMCBlock;

/**
 * Created by Mouse on 2016/7/10.
 */
@Mod(modid = UDMCore.MODID,name = UDMCore.NAME,version = UDMCore.VERSION)
public class UDMCore {

    public static final String MODID = "udmcore";
    public static final String NAME = "UDMCore";
    public static final String VERSION = "1.0.0";

    @SidedProxy(clientSide = "team.unstudio.udmc.core.client.ClientProxy", serverSide = "team.unstudio.udmc.core.common.CommonProxy")
    public static CommonProxy proxy;

    @Mod.Instance("udmcore")
    public static UDMCore instance;

    static public UDMCBlock test = new UDMCBlock(Material.iron,"test");

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.preInit(event);
        test.setRegisterName("test");
        test.register();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event){
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event){
        proxy.postInit(event);
    }
}
