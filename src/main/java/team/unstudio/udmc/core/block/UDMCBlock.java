package team.unstudio.udmc.core.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Created by winston_wang on 16/8/21.
 */
public class UDMCBlock extends Block {
    private String regisrer_name;

    public UDMCBlock(Material p_i45394_1_, String block_name) {
        super(p_i45394_1_);
        setBlockName(block_name);
    }
    public final void setRegisterName(String name){
        regisrer_name = name;
    }
    public final String getRegisterName(){
        return regisrer_name;
    }
    public final void register(){
        GameRegistry.registerBlock(this,regisrer_name);
    }
}
