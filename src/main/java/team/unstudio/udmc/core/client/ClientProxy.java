package team.unstudio.udmc.core.client;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import team.unstudio.udmc.core.common.CommonProxy;

/**
 * Created by Mouse on 2016/7/10.
 */
public class ClientProxy extends CommonProxy{

    public void preInit(FMLPreInitializationEvent event) {
        super.preInit(event);
    }

    public void init(FMLInitializationEvent event){
        super.init(event);
    }

    public void postInit(FMLPostInitializationEvent event){
        super.postInit(event);
    }
}
